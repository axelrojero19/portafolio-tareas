from django.db import models


class City(models.Model):
    id = models.AutoField(primary_key=True)  
    ciudad = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'City'

class Stadium(models.Model):
    num = models.AutoField(primary_key=True)
    estadio = models.IntegerField(blank=True, null=True)  
    nombre = models.CharField(max_length=128, blank=True, null=True)
    description = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Stadium'

class Team(models.Model):
    codigo = models.AutoField(primary_key=True)
    equipo = models.CharField(max_length=128, blank=True, null=True)
    liga = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Team'

    